# dame

dependency adder for module edna

This code is heavily under construction, and will probably break things. It is published here because I want to discuss it with some other people.
As I know nearly nothing about elisp, I shamelessly cloned the org-superlink package and wanted to throw away everything that is not needed for my purpose.
Right now it sems to work, but I don't know exactly why, so be warned.

As it is not available as a package I simly threw all 3 elisp files in my lisp directory and loeded them from my init.el like this:

    (load (concat user-emacs-directory "lisp/dame.el"))
    (load (concat user-emacs-directory "lisp/dame-org-rifle.el"))
    (load (concat user-emacs-directory "lisp/dame-org-ql.el"))

after that you can call it with 

    M-x dame-link

