;;; dame.el --- Dependency adding for module edna -*- lexical-binding: t; -*-

;; Copyright (C) 2020  DrOps

;; Author: DrOps <dr.ops@mailbox.org>
;; Version: 0.1
;; Package-Requires: (org)
;; URL: https://codeberg.org/dr.ops/dame
;; Keywords: convenience, edna

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; The most overly ambitiously named package to help you make dependencies for org-edna.
;;
;; I should describe how it actually works here.

;;; Code:

(require 'org)

(defvar dame-search-function "helm-org-rifle"
  "The interface to use for finding target links.
This can be a string with one of the values 'helm-org-ql',
'helm-org-rifle', or a custom function.  If you provide a custom
function it will be called with the `point` at the location the link
should be inserted.  The only other requirement is that it should call
the function `dame--insert-link' with a marker to the target link.  AKA
the place you want the backlink.

Using 'helm-org-ql' or 'helm-org-rifle' will also add a new action to
the respective action menu.

See the function `dame-link-search-interface-ql' or for an example.")

(declare-function dame-link-search-interface-ql "ext:dame-org-ql")
(declare-function dame-link-search-interface-rifle "ext:dame-org-rifle")

(defun dame-search-function ()
  "Call the search interface specified in `dame-search-function'."
  (cond ((string= dame-search-function "helm-org-ql")
	 (require 'dame-org-ql)
	 (dame-link-search-interface-ql))
	((string= dame-search-function "helm-org-rifle")
	 (require 'dame-org-rifle)
	 (dame-link-search-interface-rifle))
	(t (funcall dame-search-function))))


(defun dame-default-description-formatter (link desc)
  "Return a string to use as the link desciption.
LINK is the link target.  DESC is the provided desc."
  (let ((p dame-default-description-formatter))
    (cond ((equal p nil) (or desc link))
	  ((stringp p) (or desc p))
	  ((fboundp p) (funcall p link desc))
	  (t desc))))

(defun dame-backlink-into-drawer ()
  "Name of the backlink drawer, as a string, or nil.
This is the value of `dame-backlink-into-drawer'.  However, if the
current entry has or inherits a BACKLINK_INTO_DRAWER property, it will
be used instead of the default value."
  (let ((p (org-entry-get nil "BACKLINK_INTO_DRAWER" 'inherit t)))
    (cond ((equal p "nil") nil)
	  ((equal p "t") "PROPERTIES")
	  ((stringp p) p)
	  (p "PROPERTIES")
	  ((stringp dame-backlink-into-drawer) dame-backlink-into-drawer)
	  (dame-backlink-into-drawer "PROPERTIES"))))

(defun dame-insert-trigger (desc)
  (org-set-property "TRIGGER" (format "ids(%s) todo!(NEXT) scheduled!(.)" desc))
  )    

(defun dame-insert-blocker (desc)
  "Insert backlink to LINK with DESC.
Where the backlink is placed is determined by the variable `dame-backlink-into-drawer'."
    (org-set-property "BLOCKER" (format "ids(%s)" desc))
	)

(defun dame--insert-link (target)
  "Insert link to marker TARGET at current `point`, and create backlink to here.
Only create backlinks in files in `org-mode' or a derived mode, otherwise just
act like a normal link."
  (let ((origin (drops-id-get-or-generate)))
    (with-current-buffer (marker-buffer target)
      (save-excursion
		(goto-char (marker-position target))
		(when (derived-mode-p 'org-mode)
		  (dame-insert-blocker origin)
		  )
		(setq destination (drops-id-get-or-generate))
		)
	  )
	)
    (dame-insert-trigger destination)
  )

;;;###autoload
(defun dame-link ()
  "Insert a link and add a backlink to the target heading."
  (interactive)
  (dame-search-function))

(provide 'dame)

;;; dame.el ends here
